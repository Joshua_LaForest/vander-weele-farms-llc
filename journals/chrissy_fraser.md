## Oct 3, 2022

So far, I worked on:

* Updated interface design in excalidraw
* Endpoints for products; PUT & DELETE
* Discussed UI design for the orders page
* Discussed Admin vs Customer views
  
The group began delegating roles, Megan & I will be working on the Login, Logout, & Sign-Up portions.